package week3.day1;

import java.util.ArrayList;
import java.util.List;

public class LearningCollections {
	public static void main(String[] args) {
		String[] name = new String[10];
		List<String> list = new ArrayList<>();
		list.add("john");
		list.add("sophi");
		list.add("cathy");
		System.out.println(list);
		System.out.println(list.size());
		System.out.println(list.get(0));
		list.add(1,"sheeja");
		System.out.println(list);
		list.set(2, "sophia");
		System.out.println(list);
		list.remove("john");
		System.out.println(list);
	}

}

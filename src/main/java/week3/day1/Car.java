package week3.day1;

public class Car extends Vehicle {
	
	public void openDoor() {
		applyBrake();
		soundHorn();
		System.out.println("open door -> car");
	}

}

package week3.day1;

import java.util.ArrayList;
import java.util.List;

public class LearningSet {
	public static void main(String[] args) {
		String[] input = {"HCL","Wipro","Aspire Systems","CTS"};
//		Declare a String array and add the Strings values as (HCL, Wipro,  Aspire Systems, CTS)		
        List<String> list = new ArrayList<>();
        list.add("HCL");
        list.add("Wipro");
        list.add("Aspire Systems");
        list.add("CTS");
 //		get the length of the array		
        System.out.println(list);
//		sort the array			
        list.sort(list);
//		Iterate it in the reverse order

//		print the array
		
//		Required Output: Wipro, HCL , CTS, Aspire Systems

	}

}

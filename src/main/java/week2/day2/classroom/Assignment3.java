package week2.day2.classroom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment3 {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://acme-test.uipath.com/login");
		driver.manage().window().maximize();
		WebElement elementUserName = driver.findElement(By.id("email"));
		elementUserName.sendKeys("kumar.testleaf@gmail.com");
		WebElement elementpwd = driver.findElement(By.id("password"));
		elementpwd.sendKeys("leaf@12");
		driver.findElement(By.xpath("//button[contains(@type,'submit')]")).click();	
		System.out.println(driver.getTitle());
		driver.findElement(By.partialLinkText("Log")).click();	
		driver.close();
	}

}

package week2.day2.classroom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assignment1Facebook {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://en-gb.facebook.com/");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text() ='Create New Account']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[contains (@name, 'firstname')]")).sendKeys("John");
		driver.findElement(By.xpath("//input[contains (@name, 'lastname')]")).sendKeys("Clament");
		driver.findElement(By.xpath("//input[contains (@name, 'reg_email__')]")).sendKeys("9999900000");
		driver.findElement(By.xpath("//input[contains (@name, 'reg_passwd__')]")).sendKeys("test@123");
		WebElement day = driver.findElement(By.id("day"));
		Select dd = new Select(day);
		dd.selectByValue("12");
		WebElement mon = driver.findElement(By.id("month"));
		Select mm = new Select(mon);
		mm.selectByVisibleText("Jun");
		WebElement yer = driver.findElement(By.id("year"));
		Select yy = new Select(yer);
		yy.selectByVisibleText("1984");
		driver.findElement(By.xpath("//label[text() ='Female']")).click();
		
	
	}

}

package week2.day2.classroom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assignment2CreateContact {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		WebElement elementUserName = driver.findElement(By.id("username"));
		elementUserName.sendKeys("Demosalesmanager");
		WebElement elementpwd = driver.findElement(By.id("password"));
		elementpwd.sendKeys("crmsfa");
		WebElement button = driver.findElement(By.className("decorativeSubmit"));
		button.click();
		driver.findElement(By.partialLinkText("CRM/SFA")).click();
		driver.findElement(By.linkText("Leads")).click();
		driver.findElement(By.partialLinkText("Create")).click();
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys("cts");
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys("John");
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys("Clament");
		driver.findElement(By.id("createLeadForm_firstNameLocal")).sendKeys("JC");
		driver.findElement(By.id("createLeadForm_lastNameLocal")).sendKeys("JC1");
		driver.findElement(By.id("createLeadForm_departmentName")).sendKeys("QA");
		driver.findElement(By.id("createLeadForm_description")).sendKeys("Test update");
		driver.findElement(By.id("createLeadForm_primaryEmail")).sendKeys("johnclament2000@gmail.com");
		WebElement dropdown = driver.findElement(By.id("createLeadForm_generalStateProvinceGeoId"));
		Select dd = new Select(dropdown);
		dd.selectByVisibleText("New York");
		driver.findElement(By.className("smallSubmit")).click();
		driver.findElement(By.linkText("Edit")).click();
		driver.findElement(By.id("updateLeadForm_description")).clear();
		driver.findElement(By.id("updateLeadForm_importantNote")).sendKeys("Test update notes");
		driver.findElement(By.xpath("//input[contains (@name,'submitButton')]")).click();
		//driver.findElement(By.id("sectionHeaderTitle_leads")).getText();
		System.out.println(driver.findElement(By.id("sectionHeaderTitle_leads")).getText());
}

}

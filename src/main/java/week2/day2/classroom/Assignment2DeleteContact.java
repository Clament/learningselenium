package week2.day2.classroom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment2DeleteContact {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		WebElement elementUserName = driver.findElement(By.id("username"));
		elementUserName.sendKeys("Demosalesmanager");
		WebElement elementpwd = driver.findElement(By.id("password"));
		elementpwd.sendKeys("crmsfa");
		WebElement button = driver.findElement(By.className("decorativeSubmit"));
		button.click();
		driver.findElement(By.partialLinkText("CRM/SFA")).click();
		driver.findElement(By.linkText("Leads")).click();
		driver.findElement(By.partialLinkText("Find")).click();	
		driver.findElement(By.linkText("Phone")).click();
		driver.findElement(By.xpath("//input[contains(@name,'phoneAreaCode')]")).sendKeys("044");
		driver.findElement(By.xpath("//input[contains(@name,'phoneNumber')]")).sendKeys("9840079314");
		driver.findElement(By.xpath("//button[text()='Find Leads']")).click();
		Thread.sleep(2000);
		String text = driver.findElement(By.linkText("10732")).getText();
		System.out.println(text);
		driver.findElement(By.linkText(text)).click();
		driver.findElement(By.linkText("Delete")).click();
		driver.findElement(By.partialLinkText("Find")).click();	
		driver.findElement(By.xpath("//input[contains(@name,'id')] ")).sendKeys(text);
		driver.findElement(By.xpath("//button[text()='Find Leads']")).click();
		Thread.sleep(2000);
		WebElement message = driver.findElement(By.xpath("//div[text()='No records to display']"));
		if (message.getText() .equals("No records to display")) {
			System.out.println("Record deleted successfuly");
		}
			else {
				System.out.println("Record not deleted successfuly");
			}
		driver.close();
		}
		
	}

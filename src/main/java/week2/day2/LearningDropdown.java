package week2.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearningDropdown {

	public static void main(String[] args) {
		//System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		WebElement elementUserName = driver.findElement(By.id("username"));
		elementUserName.sendKeys("Demosalesmanager");
		WebElement elementpwd = driver.findElement(By.id("password"));
		elementpwd.sendKeys("crmsfa");
		WebElement button = driver.findElement(By.className("decorativeSubmit"));
		button.click();
		driver.findElement(By.partialLinkText("CRM/SFA")).click();
		driver.findElement(By.linkText("Leads")).click();
		driver.findElement(By.partialLinkText("Create")).click();
		WebElement dropdown = driver.findElement(By.id("createLeadForm_dataSourceId"));
		Select dd = new Select(dropdown);
		dd.selectByVisibleText("Conference");
		WebElement dropdown1 = driver.findElement(By.id("createLeadForm_marketingCampaignId"));
		Select dd1 = new Select(dropdown1);
		dd1.selectByValue("DEMO_MKTG_CAMP");
		WebElement dropdown2 = driver.findElement(By.id("createLeadForm_industryEnumId"));
		Select dd2 = new Select(dropdown2);
		dd2.selectByIndex(2);
		
		
		
	}

}

package week2.day1.classroom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestCase3 {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		WebElement elementUserName = driver.findElement(By.id("username"));
		elementUserName.sendKeys("Demosalesmanager");
		WebElement elementpwd = driver.findElement(By.id("password"));
		elementpwd.sendKeys("crmsfa");
		WebElement button = driver.findElement(By.className("decorativeSubmit"));
		button.click();
		driver.findElement(By.partialLinkText("CRM/SFA")).click();
		driver.findElement(By.linkText("Leads")).click();
		driver.findElement(By.partialLinkText("Create")).click();
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys("cts");
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys("John");
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys("Clament");
		WebElement dropdown1 = driver.findElement(By.id("createLeadForm_marketingCampaignId"));
		dropdown1.click();
		dropdown1.sendKeys("cc");
		driver.findElement(By.id("createLeadForm_firstNameLocal")).sendKeys("JC");
		driver.findElement(By.id("createLeadForm_lastNameLocal")).sendKeys("JC1");
		driver.findElement(By.id("createLeadForm_personalTitle")).sendKeys("Mr");
		driver.findElement(By.id("createLeadForm_generalProfTitle")).sendKeys("Mr");
		driver.findElement(By.id("createLeadForm_departmentName")).sendKeys("QA");
		driver.findElement(By.id("createLeadForm_annualRevenue")).sendKeys("1");
		WebElement dropdown2 = driver.findElement(By.id("createLeadForm_industryEnumId"));
		dropdown2.click();
		dropdown2.sendKeys("f");
		WebElement dropdown3 = driver.findElement(By.id("createLeadForm_ownershipEnumId"));
		dropdown3.click();
		dropdown3.sendKeys("ss");
		driver.findElement(By.id("createLeadForm_numberEmployees")).sendKeys("10");
		driver.findElement(By.id("createLeadForm_sicCode")).sendKeys("002");
		driver.findElement(By.id("createLeadForm_tickerSymbol")).sendKeys("002");
		driver.findElement(By.id("createLeadForm_description")).sendKeys("Test update");
		driver.findElement(By.id("createLeadForm_importantNote")).sendKeys("Test update notes");
		driver.findElement(By.id("createLeadForm_primaryPhoneAreaCode")).sendKeys("044");
		driver.findElement(By.id("createLeadForm_primaryPhoneExtension")).sendKeys("044");
		driver.findElement(By.id("createLeadForm_primaryEmail")).sendKeys("johnclament2000@gmail.com");
		driver.findElement(By.id("createLeadForm_primaryPhoneNumber")).sendKeys("9840079314");
		driver.findElement(By.id("createLeadForm_primaryWebUrl")).sendKeys("https://test");
		driver.findElement(By.id("createLeadForm_generalToName")).sendKeys("John Clament");
		driver.findElement(By.id("createLeadForm_generalAttnName")).sendKeys("John Clament");
		driver.findElement(By.id("createLeadForm_generalAddress1")).sendKeys("Test update address1");
		driver.findElement(By.id("createLeadForm_generalAddress2")).sendKeys("Test update address2");
		driver.findElement(By.id("createLeadForm_generalCity")).sendKeys("Chennai");
		driver.findElement(By.id("createLeadForm_generalPostalCode")).sendKeys("600053");
		driver.findElement(By.id("createLeadForm_generalPostalCodeExt")).sendKeys("1234");		
		WebElement dropdown = driver.findElement(By.id("createLeadForm_generalStateProvinceGeoId"));
		dropdown.click();
		dropdown.sendKeys("a");
		driver.findElement(By.className("smallSubmit")).click();
	}

}

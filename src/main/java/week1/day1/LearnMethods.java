package week1.day1;

public class LearnMethods {
	
	public static void main(String[] args) {
		System.out.println("Hello World");
		// calling methods inside main
		//className object = new className()
		
		LearnMethods object = new LearnMethods();
		//syntax to call method now
		
		object.printName();
		int output = object.getCreditCardPinNo();
		System.out.println(output);
		object.printMyName("John Clament from print");
		
	}
	
	public void printName() {
		System.out.println("John Clament");
	}
	
	private int getCreditCardPinNo () {
		int pin = 1234;
		return pin;
	}
	void printMyName(String name) {
		System.out.println(name);
	}

}

package week1.day1;

public class LearningConditionalStatement {
	
	// print numbers from 1 to 100
/**	public static void main(String[] args) {
		for (int i = 1; i <= 100; i++) {
			System.out.println(i);
		}
	} **/
	

	//print odd numbers from 1 to 100
/**	public static void main(String[] args) {
		
		for (int i = 1; i <= 100; i++) {
			if ( i % 2 != 0) {
			System.out.println(i);
		}
	}

} **/
	
	//print square of even numbers from 59-38
/**	public static void main(String[] args) {
		
		for (int i = 59; i >=38 ; i --) {
			if (i %2 ==0 ) {
				int j = i * i;
				System.out.println(j);
			}
			
		}
	} **/
	
	// print range of the given number
	public static void main(String[] args) {
		int range = 5;
		int result = 0;
		for ( int i= 1; i<=range; i++) {
			result = result +i;
		}
			System.out.println(result);
		}

}
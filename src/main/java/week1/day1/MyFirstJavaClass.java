package week1.day1;

public class MyFirstJavaClass {
	
// this is a single line comment
	
/**
  This is multi line comment
  hi 
  for testing
 **/
	
	byte rollNumber = 1;
	short empId = 12345;
	int insuranNumber = 123456789;
	long mobileNumber = 1234567890l;
	float bankBalance = 12345.50f;
	double bankBalance1 = 12345.56;
	boolean hadLunch = true;
	char lastName = 's';
	String fullName ="John Clament";
	
	

}

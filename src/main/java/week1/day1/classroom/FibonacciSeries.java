package week1.day1.classroom;

public class FibonacciSeries {

	
	public static void main(String[] args) {
		int firstNum = 0;
		int secondNum = 1;
		int range = 8;
		
		for (int i =1 ; i <=range; i++) {
			System.out.println(firstNum);
			
			int n = firstNum + secondNum;
			firstNum = secondNum;
			secondNum = n;
			
		}
		
	}

}
